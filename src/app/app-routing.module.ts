import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroComponent } from './views/home/cadastro/cadastro.component';
import { HomeComponent } from './views/home/home.component';
import { marcacaoComponent } from './views/home/marcacao/marcacao/marcacaoComponent';


const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'marcacao', component: marcacaoComponent
  },
  {
    path: 'cadastro', component: CadastroComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
